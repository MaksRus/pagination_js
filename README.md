# Pagination en JS (ou TS)

## Rappel

Découpez votre code de façon bien structurée !
(le code ci-dessous est juste à titre d'exemple)

```js
// Global
const database = [];
const index = 1;

// DOM
const userTable = document.querySelector('#user-list');
const nextBtn = document.querySelector('btn#btn-next');

// Functions
function showFirstPage() {
    //mon code
}

function showNextPage() {
    //mon code
}

// Events
nextBtn.addEventListener('click', showNextPage);

// Init
showFirstPage()

```

## La base

Afficher un utilisateur de la "base de données" dans le tableau #user-list sous forme
d'élément TR.

## Toute la BDD

Afficher l'intégralité des utilisateurs de la base de données dans le tableau (30 éléments TR
à générer donc).

## Filtrer

N'afficher que les 5 premiers utilisateurs...
puis les 5 d'après...?? etc

## Intégration de la pagination

Automatiser le processus en reliant les boutons de la pagination à vos fonctions !