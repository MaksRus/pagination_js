import { database } from '../data/db.js';

//Variables
const ROWS_BY_PAGE = 5;
let currentPage = 1;
let start = 0;
let nbOfPages = Math.ceil(database.length / ROWS_BY_PAGE);


//HTML ELEMENTS
const userList = document.querySelector('#user-list');
const prevBtn = document.querySelector('#prev');
const nextBtn = document.querySelector('#next');
const firstBtn = document.querySelector('#to-first');
const lastBtn = document.querySelector('#to-last');
const currentPageEl = document.querySelector('#current-page');
const lastPageEl = document.querySelector('#last-page');


//Functions
function generateUserRow(user) {
    return `<tr> \
        <td>${user.id}</td>\
        <td>${user.name}</td>\
        <td>${user.age}</td>\
        <td>${user.email}</td>\
        <td>${user.gender}</td>\
    </tr>`;
}

function displayUsers() {
    userList.innerHTML = ""; // Reseting display
    updateNav();
    const end = start + ROWS_BY_PAGE;
    for (let i = start; i < end; i++) {
        if(i < database.length) {
            userList.innerHTML += generateUserRow(database[i]);
        }
    }
}

function updateNav() {
    currentPageEl.innerText = currentPage;
    lastPageEl.innerText = nbOfPages;
}

function prevPage() {
    if(start - ROWS_BY_PAGE < 0) return;
    start -= ROWS_BY_PAGE;
    currentPage--;
    displayUsers();
}

function nextPage() {
    if(start + ROWS_BY_PAGE >= database.length) return;
    start += ROWS_BY_PAGE;
    currentPage++;
    displayUsers();
}

function firstPage() {
    start = 0;
    currentPage = 1;
    displayUsers();
}

function lastPage() {
    start = (nbOfPages * ROWS_BY_PAGE) - ROWS_BY_PAGE;
    currentPage = nbOfPages;
    displayUsers();
}

//Event Listeners
firstBtn.addEventListener('click', firstPage);
lastBtn.addEventListener('click', lastPage);
prevBtn.addEventListener('click', prevPage);
nextBtn.addEventListener('click', nextPage);

//Init
displayUsers();