"use strict";
const database = [
    {
        "id": 0,
        "name": "Dejesus Hall",
        "age": 31,
        "email": "dejesushall@genesynk.com",
        "gender": "male",
    },
    {
        "id": 1,
        "name": "Sharron Rose",
        "age": 26,
        "email": "sharronrose@genesynk.com",
        "gender": "female",
    },
    {
        "id": 2,
        "name": "Reese Dickson",
        "age": 24,
        "email": "reesedickson@genesynk.com",
        "gender": "male",
    },
    {
        "id": 3,
        "name": "Knox Meyer",
        "age": 27,
        "email": "knoxmeyer@genesynk.com",
        "gender": "male",
    },
    {
        "id": 4,
        "name": "Christy Prince",
        "age": 40,
        "email": "christyprince@genesynk.com",
        "gender": "female",
    },
    {
        "id": 5,
        "name": "Vicky Burke",
        "age": 30,
        "email": "vickyburke@genesynk.com",
        "gender": "female",
    },
    {
        "id": 6,
        "name": "Chelsea Sykes",
        "age": 22,
        "email": "chelseasykes@genesynk.com",
        "gender": "female",
    },
    {
        "id": 7,
        "name": "Mcguire Forbes",
        "age": 32,
        "email": "mcguireforbes@genesynk.com",
        "gender": "male",
    },
    {
        "id": 8,
        "name": "Maynard Knight",
        "age": 26,
        "email": "maynardknight@genesynk.com",
        "gender": "male",
    },
    {
        "id": 9,
        "name": "Clara Washington",
        "age": 29,
        "email": "clarawashington@genesynk.com",
        "gender": "female",
    },
    {
        "id": 10,
        "name": "Wolfe Livingston",
        "age": 20,
        "email": "wolfelivingston@genesynk.com",
        "gender": "male",
    },
    {
        "id": 11,
        "name": "Marva Maynard",
        "age": 31,
        "email": "marvamaynard@genesynk.com",
        "gender": "female",
    },
    {
        "id": 12,
        "name": "Berger Franklin",
        "age": 30,
        "email": "bergerfranklin@genesynk.com",
        "gender": "male",
    },
    {
        "id": 13,
        "name": "Georgina Vazquez",
        "age": 36,
        "email": "georginavazquez@genesynk.com",
        "gender": "female",
    },
    {
        "id": 14,
        "name": "Fisher Chase",
        "age": 38,
        "email": "fisherchase@genesynk.com",
        "gender": "male",
    },
    {
        "id": 15,
        "name": "Atkins Rutledge",
        "age": 25,
        "email": "atkinsrutledge@genesynk.com",
        "gender": "male",
    },
    {
        "id": 16,
        "name": "Parks Bond",
        "age": 34,
        "email": "parksbond@genesynk.com",
        "gender": "male",
    },
    {
        "id": 17,
        "name": "Barnes Kline",
        "age": 39,
        "email": "barneskline@genesynk.com",
        "gender": "male",
    },
    {
        "id": 18,
        "name": "Callahan Mullen",
        "age": 22,
        "email": "callahanmullen@genesynk.com",
        "gender": "male",
    },
    {
        "id": 19,
        "name": "Conrad Reese",
        "age": 39,
        "email": "conradreese@genesynk.com",
        "gender": "male",
    },
    {
        "id": 20,
        "name": "Page Stark",
        "age": 31,
        "email": "pagestark@genesynk.com",
        "gender": "male",
    },
    {
        "id": 21,
        "name": "Elizabeth Buck",
        "age": 32,
        "email": "elizabethbuck@genesynk.com",
        "gender": "female",
    },
    {
        "id": 22,
        "name": "Angelita Kelley",
        "age": 34,
        "email": "angelitakelley@genesynk.com",
        "gender": "female",
    },
    {
        "id": 23,
        "name": "Jannie Atkins",
        "age": 30,
        "email": "jannieatkins@genesynk.com",
        "gender": "female",
    },
    {
        "id": 24,
        "name": "Lynnette Bryant",
        "age": 20,
        "email": "lynnettebryant@genesynk.com",
        "gender": "female",
    },
    {
        "id": 25,
        "name": "Lisa Kirk",
        "age": 40,
        "email": "lisakirk@genesynk.com",
        "gender": "female",
    },
    {
        "id": 26,
        "name": "Marian Calderon",
        "age": 31,
        "email": "mariancalderon@genesynk.com",
        "gender": "female",
    },
    {
        "id": 27,
        "name": "Christa Dennis",
        "age": 24,
        "email": "christadennis@genesynk.com",
        "gender": "female",
    },
    {
        "id": 28,
        "name": "Louise Hahn",
        "age": 22,
        "email": "louisehahn@genesynk.com",
        "gender": "female",
    },
    {
        "id": 29,
        "name": "Orr Roberson",
        "age": 40,
        "email": "orrroberson@genesynk.com",
        "gender": "male",
    }
];
class User {
    constructor(id, name, age, email, gender) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.email = email;
        this.gender = gender;
    }
    greetings() {
        console.log(`Hello, my name is ${this.name}`);
    }
    generateTr() {
        const row = document.createElement('tr');
        Object.keys(this).forEach((key) => {
            const cell = document.createElement('td');
            cell.innerText = this[key];
            row.appendChild(cell);
        });
        return row;
    }
}
//Creating the User list
const users = [];
database.forEach(data => {
    const user = new User(data.id, data.name, data.age, data.email, data.gender);
    users.push(user);
});
//Variables
const ROWS_BY_PAGE = 5;
let currentPage = 1;
let nbOfPages = Math.ceil(users.length / ROWS_BY_PAGE);
//HTML ELEMENTS
const userList = document.querySelector('#user-list');
const prevBtn = document.querySelector('#prev');
const nextBtn = document.querySelector('#next');
const firstBtn = document.querySelector('#to-first');
const lastBtn = document.querySelector('#to-last');
const currentPageEl = document.querySelector('#current-page');
const lastPageEl = document.querySelector('#last-page');
//Functions
/**
 * Display required page
 * @param {number} page
 * @returns
 */
const displayPage = (page) => {
    if (page <= 0 || page > nbOfPages)
        return;
    userList.innerHTML = ""; // Reseting display
    const firstIndex = ROWS_BY_PAGE * (page - 1);
    currentPage = page;
    updateNav();
    for (let i = firstIndex; i < firstIndex + ROWS_BY_PAGE; i++) {
        if (i >= database.length)
            return;
        userList.append(users[i].generateTr());
    }
};
/**
 * Updates navigation info in view
 */
const updateNav = () => {
    currentPageEl.innerText = currentPage.toString();
    lastPageEl.innerText = nbOfPages.toString();
};
//Event Listeners
firstBtn.addEventListener('click', () => {
    displayPage(1); // First page
});
lastBtn.addEventListener('click', () => {
    displayPage(nbOfPages); //Last page
});
prevBtn.addEventListener('click', () => {
    displayPage(currentPage - 1); //Previous page
});
nextBtn.addEventListener('click', () => {
    displayPage(currentPage + 1); //Next page
});
//Init
displayPage(currentPage);
