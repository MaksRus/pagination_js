import { database } from '../data/db.js';

//Variables
const ROWS_BY_PAGE = 7;
let currentPage = 1;
let nbOfPages = Math.ceil(database.length / ROWS_BY_PAGE);


//HTML ELEMENTS
const userList = document.querySelector('#user-list');
const prevBtn = document.querySelector('#prev');
const nextBtn = document.querySelector('#next');
const firstBtn = document.querySelector('#to-first');
const lastBtn = document.querySelector('#to-last');
const currentPageEl = document.querySelector('#current-page');
const lastPageEl = document.querySelector('#last-page');


//Functions

/**
 * Generates <tr> node Element based on user's data
 * @param {object} user 
 * @returns HTMLRowElement
 */
const generateUserRow = user => {
    const row = document.createElement('tr');
    
    Object.keys(user).forEach(key => {
        const cell = document.createElement('td');
        cell.innerText = user[key];
        row.appendChild(cell);
    });

    return row;
} 

/**
 * Display required page
 * @param {number} page 
 * @returns 
 */
const displayPage = page => {
    if(page <= 0 || page > nbOfPages) return;

    userList.innerHTML = ""; // Reseting display
    const firstIndex = ROWS_BY_PAGE * (page - 1);
    currentPage = page;
    updateNav();

    for (let i = firstIndex; i < firstIndex + ROWS_BY_PAGE; i++) {
        if(i >= database.length) return;
        userList.append(generateUserRow(database[i]));
    }
}

/**
 * Updates navigation info in view
 */
const updateNav = () => {
    currentPageEl.innerText = currentPage;
    lastPageEl.innerText = nbOfPages;
}

//Event Listeners
firstBtn.addEventListener('click', () => {
    displayPage(1); // First page
});
lastBtn.addEventListener('click', () => {
    displayPage(nbOfPages); //Last page
});
prevBtn.addEventListener('click', () => {
    displayPage(currentPage - 1); //Previous page
});
nextBtn.addEventListener('click', () => {
    displayPage(currentPage + 1); //Next page
});

//Init
displayPage(1);