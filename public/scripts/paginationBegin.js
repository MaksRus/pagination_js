"use strict";
// interface User {
//     id: number,
//     name: string,
//     age: number,
//     email: string,
//     gender: string,
// }
// const database: User[] = [
//     {
//         "id": 0,
//         "name": "Dejesus Hall",
//         "age": 31,
//         "email": "dejesushall@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 1,
//         "name": "Sharron Rose",
//         "age": 26,
//         "email": "sharronrose@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 2,
//         "name": "Reese Dickson",
//         "age": 24,
//         "email": "reesedickson@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 3,
//         "name": "Knox Meyer",
//         "age": 27,
//         "email": "knoxmeyer@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 4,
//         "name": "Christy Prince",
//         "age": 40,
//         "email": "christyprince@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 5,
//         "name": "Vicky Burke",
//         "age": 30,
//         "email": "vickyburke@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 6,
//         "name": "Chelsea Sykes",
//         "age": 22,
//         "email": "chelseasykes@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 7,
//         "name": "Mcguire Forbes",
//         "age": 32,
//         "email": "mcguireforbes@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 8,
//         "name": "Maynard Knight",
//         "age": 26,
//         "email": "maynardknight@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 9,
//         "name": "Clara Washington",
//         "age": 29,
//         "email": "clarawashington@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 10,
//         "name": "Wolfe Livingston",
//         "age": 20,
//         "email": "wolfelivingston@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 11,
//         "name": "Marva Maynard",
//         "age": 31,
//         "email": "marvamaynard@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 12,
//         "name": "Berger Franklin",
//         "age": 30,
//         "email": "bergerfranklin@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 13,
//         "name": "Georgina Vazquez",
//         "age": 36,
//         "email": "georginavazquez@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 14,
//         "name": "Fisher Chase",
//         "age": 38,
//         "email": "fisherchase@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 15,
//         "name": "Atkins Rutledge",
//         "age": 25,
//         "email": "atkinsrutledge@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 16,
//         "name": "Parks Bond",
//         "age": 34,
//         "email": "parksbond@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 17,
//         "name": "Barnes Kline",
//         "age": 39,
//         "email": "barneskline@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 18,
//         "name": "Callahan Mullen",
//         "age": 22,
//         "email": "callahanmullen@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 19,
//         "name": "Conrad Reese",
//         "age": 39,
//         "email": "conradreese@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 20,
//         "name": "Page Stark",
//         "age": 31,
//         "email": "pagestark@genesynk.com",
//         "gender": "male",
//     },
//     {
//         "id": 21,
//         "name": "Elizabeth Buck",
//         "age": 32,
//         "email": "elizabethbuck@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 22,
//         "name": "Angelita Kelley",
//         "age": 34,
//         "email": "angelitakelley@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 23,
//         "name": "Jannie Atkins",
//         "age": 30,
//         "email": "jannieatkins@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 24,
//         "name": "Lynnette Bryant",
//         "age": 20,
//         "email": "lynnettebryant@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 25,
//         "name": "Lisa Kirk",
//         "age": 40,
//         "email": "lisakirk@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 26,
//         "name": "Marian Calderon",
//         "age": 31,
//         "email": "mariancalderon@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 27,
//         "name": "Christa Dennis",
//         "age": 24,
//         "email": "christadennis@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 28,
//         "name": "Louise Hahn",
//         "age": 22,
//         "email": "louisehahn@genesynk.com",
//         "gender": "female",
//     },
//     {
//         "id": 29,
//         "name": "Orr Roberson",
//         "age": 40,
//         "email": "orrroberson@genesynk.com",
//         "gender": "male",
//     }
// ];
// //HTML ELEMENTS
// const userList = document.querySelector('#user-list') as HTMLTableSectionElement;
// //Functions
// function generateUserRow(user: User): string {
//     return `<tr> \
//         <td>${user.id}</td>\
//         <td>${user.name}</td>\
//         <td>${user.age}</td>\
//         <td>${user.email}</td>\
//         <td>${user.gender}</td>\
//     </tr>`;
// }
// function displayAll(): void {
//     for (let i = 0; i < database.length; i++) {
//         userList.innerHTML += generateUserRow(database[i]);
//     }
// }
// //Event Listeners
// //Init
// document.addEventListener('DOMContentLoaded', displayAll)
// displayAll()
